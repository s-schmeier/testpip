#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest>=3', ]

# Note: the __program__ variable is set in __init__.py.
# it determines the name of the package/final command line tool.
from evopipe import __version__, __program__, __author__, __email__

setup(
    name=__program__,
    version=__version__,
    author=__author__,
    author_email=__email__,
    python_requires='>=3.6',
    packages=[__program__],
    description="An Snakemake command line interface for evopipe.",
    long_description=readme + '\n\n' + history,
    install_requires=["snakemake==5.32.0", "xlrd==2.0.1", "openpyxl==3.0.6", "click==7.1.2", "pyyaml==5.4.1"],
    test_suite='tests',
    tests_require=test_requirements,
    keywords='evopipe',
    include_package_data=True,
    setup_requires=setup_requirements,
    zip_safe=False,
    entry_points = {'console_scripts': [
        'evopipe  = evopipe.__main__:main'
        ]
    },
)
