===========
evopipe
===========

.. image:: https://readthedocs.org/projects/evopipe/badge/?version=latest
        :target: https://evopipe.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/sschmeier/evopipe/shield.svg
     :target: https://pyup.io/repos/github/sschmeier/evopipe/
     :alt: Updates


An Snakemake command line interface for evopipe.

This example bundles the Snakefile with the
command line tool, but this tool can also look
in the user's working directory for Snakefiles.

Snakemake functionality is provided through
a command line tool called `evopipe`.

Quickstart
----------

This runs through the installation and usage
of `evopipe`.

Quick install
-------------

Start by setting up a conda environment,
and install the required packages into the
environment:

.. code-block:: console
    
    $ conda env create -p /install/path -f evopipe.yaml
    $ conda activate /install/path
    $ make install


Now you can run

.. code-block:: console

    $ which evopipe


and you should see `evopipe` in your
environment's `bin/` directory.


Running evopipe
----------------


.. code-block:: console

    evopipe setup -n example
    cd example
    # Edit the config.yaml to your specific project needs
    evopipe run config.yaml


Details
-------

The entrypoint of the command line interface is
the `main()` function of `evopipe/__main__.py`.

The location of the Snakefile is `evopipe/Snakefile`.
