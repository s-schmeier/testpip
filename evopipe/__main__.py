import sys
import os
import os.path
import pathlib
import subprocess
import shutil
import yaml 
import logging
import errno

logger = logging.getLogger()

## non standard imports
## For command line args
import click
## for validating files, like samplesshet / configfile
from snakemake.utils import validate


#from . import __version__
#from . import __date__
__version__="0.1.0"
__date__="2021/01/31"
__conf__="conf/system.conf" 

def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:  # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            logger.error("Unable to copy. EXIT")
            raise


def get_package_path(path):
    ## maybe use realpath here?
    thisdir = os.path.dirname(__file__)
    fname = os.path.join(thisdir, path)
    return fname


def load_and_validate_conf(path=__conf__, schema="schemas/system.schema.yaml"):
    try:
      conffile = open(get_package_path(path))
    except IOError:
        logger.error(f"Could not open file {get_package_path(path)} . Exit")
        sys.exit(1)

    conf = yaml.safe_load(conffile)
    if not os.path.isfile(get_package_path(schema)):
        logger.error(f"Could not open file {get_package_path(schema)}. Exit")
        sys.exit(1)

    validate(conf, schema=get_package_path(schema))
    return conf


def parse_snake_config(snakeconfig, exclude_args):
    snake_args = [] 
    ## add global args to cmd, but only if the arg is not in extra_args
    for k, v in snakeconfig.items():
        arg = f"--{k}"
        ## in user submitted args?
        if arg not in exclude_args:
            ## its an arg
            if type(v) == bool:
                if v == True:
                    snake_args += [arg]
            ## emtpy arg
            elif v=="":
                continue
            ## arg + value
            else:   
                ## if the arg should point to a package config file
                if arg in ["--cluster-config"]:
                    v = get_package_path(v)

                if arg in ["--profile"]:
                    v = get_package_path(v)

                ## some values of an option can be a string of multiple items
                ## and need to be quoted
                ## e.g. --singularity-args: "--bind bla --bind yadda"
                value = str(v)
                if len(value.split(' ')) > 1:
                    value = f'"{v}"'
                else:
                    value = v
                snake_args += [arg, str(value)]
    return snake_args


def run_snakemake(samplesheet,
                  outdir,
                  configfile=None,
                  verbose=False,
                  snakefile_name='Snakefile',
                  dryrun=False,
                  extra_args=[]):
    
    ## load system conf
    if verbose:
        logger.info("Load workflow system config.")
    systemconf = load_and_validate_conf()
    
    ## find the Snakefile relative to package path
    snakefile = get_package_path(snakefile_name)

    ## base command
    cmd = ["snakemake", "--snakefile", snakefile, "-p"]
    cmd += ["--directory", os.path.realpath(outdir)]

    configs=["--config"] 
    if systemconf["container_path"] != "":
        configs += [f'containerpath={systemconf["container_path"]}']
    # add samplesheet
    configs += [f"samples={os.path.realpath(samplesheet)}"]
    # here more config params could be read from cli and added
    cmd += configs

    if verbose:
        logger.info("Parsing snakemake args.")
    ## add snakemake args based on system conf
    ## get default snakemake args but only use the ones
    ## not defined in extra_args supplied by user
    snake_args = parse_snake_config(systemconf["snakemake"], extra_args)
    cmd += snake_args

    ## now add user defined extra_args 
    for arg in extra_args:
        ## check if options are of long-form otherwise exit
        if arg[0] == '-':
            if not arg.startswith("--"):
                logger.error(f'Extra Snakemake arguments should be of long-form starting with "--": {arg}')
                sys.exit(1)

    cmd += list(extra_args)

    ## we always have a defaukt config file 
    configfiles = [get_package_path(systemconf["defaults"])]
    ## add user config (overwrites the defaults)
    if configfile:
        configfiles += [configfile]
    cmd += ["--configfile"] + configfiles

    if dryrun:
        ## make sure that --dryrun is part of args
        if "--dryrun" in cmd or "--dry-run" in cmd:
            pass
        else:
            cmd += ["--dryrun"]

    #######################################################
    ## done collecting snakemake args

    if verbose:
        logger.info('Execute snakemake command: ' + ' '.join(cmd))

    ## run snakemake
    try:
       subprocess.check_call(cmd)
    except subprocess.CalledProcessError as e:
       print(f'Error in snakemake invocation: {e}')
       return e.returncode


###########################################################
## CLI
###########################################################
@click.group()
def cli():
    """Workflow: evopipe."""
    pass


# Get workflow info
@click.command()
@click.argument('dir', type=click.Path(exists=False))
def setup(dir):
    "setup a new run."
    dest = dir
    src = os.path.realpath(get_package_path("conf/defaults.conf"))
    if os.path.exists(dest):
        logger.error(f'Destination example directory at "{dest}" already exists. EXIT.')
        sys.exit(1)

    pathlib.Path(dest).mkdir(parents=True, exist_ok=False)
    copyanything(src, dest)
    logger.info("Directory created and default config created.")


## Get workflow info
@click.command()
def info():
    "show install/config file info"
    system = load_and_validate_conf()
    defaults = os.path.realpath(get_package_path(system["defaults"]))
    slurm = os.path.realpath(get_package_path(system["snakemake"]["cluster-config"]))

    print(f"""
Workflow: snakemake_cli_boilerplate version v{__version__} ({__date__})
Package install path: {os.path.realpath(os.path.dirname(__file__))}
Workflow Snakefile: {os.path.realpath(get_package_path('Snakefile'))}
""")
    if os.path.isfile(os.path.realpath(get_package_path(__conf__))):
        print("********** Workflow system config ***********")
        print(f"** PATH: {os.path.realpath(get_package_path(__conf__))}")
        config = yaml.safe_load(open(get_package_path(__conf__)))
        for k,v in config.items():
            print(f"{k}: {v}")

    if os.path.isfile(defaults):
        print("\n********** Default workflow config ***********")
        print(f"** PATH: {defaults}")
        config = yaml.safe_load(open(defaults))
        for k,v in config.items():
            print(f"{k}: {v}")
        
    if os.path.isfile(slurm):
        print("\n********** SLURM per-rule default config *****")
        print(f"** PATH: {slurm}")
        config = yaml.safe_load(open(slurm))
        for k,v in config.items():
            print(f"{k}")
            for k2,v2 in v.items():
                print(f"  {k2}: {v2}")


## Run the workflow
@click.command(context_settings=dict(ignore_unknown_options=True,))
@click.argument('samplesheet', type=click.Path(exists=True))
@click.argument('dir', type=click.Path())
@click.option('-n',
              '--dryrun',
              is_flag=True,
              default=False,
              help='Do a dry-run.')
@click.option('--configfile',
              help='Config-file.',
              type=click.Path(exists=True))
@click.option('--verbose', is_flag=True)
@click.argument('snakemake_args', nargs=-1, type=click.UNPROCESSED)
def run(samplesheet, dir, configfile, dryrun, snakemake_args, verbose):
    """run workflow: evopipe"""
    if verbose:
        logger.info("Workflow start.")

    ## run snakemake
    run_snakemake(samplesheet=samplesheet,
                  outdir=dir,
                  configfile=configfile,
                  snakefile_name='Snakefile',
                  dryrun=dryrun,
                  verbose=verbose,
                  extra_args=snakemake_args)


cli.add_command(run)
cli.add_command(info)
cli.add_command(setup)


def main():
     # set up logger
    numeric_level = getattr(logging, 'INFO', None)
    logging.basicConfig(
        level=numeric_level,
        #format="%(asctime)s [%(levelname)8s] (%(filename)s:%(funcName)15s():%(lineno)4s): %(message)s ",
        format="%(asctime)s [%(levelname)8s]: %(message)s ",
        datefmt="%Y%m%d-%H:%M:%S",
    )
    cli()


if __name__=='__main__':
    main()
