## =============================================================================
## WORKFLOW PROJECT: evopipe
import pandas as pd
import glob, os, os.path, datetime, sys, csv, logging
from os.path import join, abspath, isfile, realpath, dirname
from snakemake.utils import validate, min_version
from sys import version_info
__version__="0.1.0"

## ============================================================================
## require minimum snakemake version ##
min_version("5.32.0")
py_version = "{}.{}.{}".format(version_info.major, version_info.minor, version_info.micro)
logger.info("RUNNING SNAKEMAKE VERSION: {} with PYTHON: {}".format(snakemake.__version__, py_version))

DIR_BASE = workflow.basedir  # install location of Snakefile
DIR_SCHEMAS = join(DIR_BASE, "schemas")  # relative to Snakefile
DIR_SCRIPTS = join(DIR_BASE, "scripts")  # relative to Snakefile
logger.info(f"RUNNING evopipe WORKFLOW VERSION: {__version__}")
logger.info(f"RUNNING WORKFLOW AT: {DIR_BASE}")

## ============================================================================
## SETUP
## ============================================================================

## LOAD VARIABLES FROM CONFIGFILE
## submit on command-line via --configfile
## or
## the conf/defaults.conf
if config=={}:
    logger.error('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

logger.info("********** Submitted config: **********")
for k,v in config.items():
    logger.info("{}: {}".format(k,v))
logger.info("***************************************")

validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))

## Setup result dirs
DIR_LOGS        = "logs"
DIR_BENCHMARKS  = "benchmarks"
DIR_RES         = "results"
DIR_CLUSTER_LOG = "logs_cluster"

## Base path to containers
DIR_CONTAINERS = config["containerpath"] 

## Workflow specific setup from config ========================================
example_extra = config["extra"]["example"]

## ============================================================================

## Reading samples from sheet

filename, file_extension = os.path.splitext(config["samples"])
if file_extension == ".xlsx":
    samples = pd.read_excel(config["samples"]).set_index("sample", drop=False, verify_integrity=True)
else:
    samples = pd.read_csv(config["samples"], sep=",").set_index("sample", drop=False, verify_integrity=True)

## Alternative reading samples from Evotec sample tables
## Needs submodule: git@di-gitlab:bix/hamburg/general/evopy.git
## see README.md
# from evopy.evopy.sampletable import convert_sampletable_to_df
# ## needs to read ET xlsx and return samples pandas df
# samples = convert_sampletable_to_df(fin=config["samples"], 
#                                     direc=config["sample_dir"], 
#                                     status=config["sample_status"])

## TODO: not working
validate(samples, schema=join(DIR_SCHEMAS, "samples.schema.yaml"))

## Testing if samples exist
## reading samplename from samplesheet
logger.info('Reading samples from samplesheet: "{}" ...'.format(config["samples"]))
for fname in samples["fq1"]:
    if not isfile(fname):
        logger.error("File '{}' from samplesheet can not be found. Make sure the file exists. Exit.\n".format(fname))
        sys.exit(1)
    
for fname in samples["fq2"]:
    if pd.isnull(fname):
        continue
    if not isfile(fname):
        logger.error("File '{}' from samplesheet can not be found. Make sure the file exists. Exit.\n".format(fname))
        sys.exit(1)


logger.info('{} samples to process'.format(len(samples["sample"])))

## ============================================================================
## FUNCTIONS
## ============================================================================
def get_fastq(wildcards):
    # does not return fq2 if it is not present
    return samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()

## ============================================================================
## SETUP FINAL TARGETS
## ============================================================================

# final files from samples
TARGETS = expand(join(DIR_RES, "final/{sample}.txt"), sample=samples["sample"])
#print(TARGETS)

## ============================================================================
## RULES
## ============================================================================

## Pseudo-rule to state the final targets, so that the whole runs
rule all:
    input:
        TARGETS
    

rule example:
    input:
        get_fastq
    output:
        join(DIR_RES, "final/{sample}.txt")
    log:
        join(DIR_LOGS, "{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "{sample}.txt")
    # container: join(DIR_CONTAINERS, "subdir/container.sif")
    threads: 2
    # params:
    #     script=join(DIR_SCRIPTS, "bla.py")
    shell:
        "sleep 5; touch {output} 2> {log}"

